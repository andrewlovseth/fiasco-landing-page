<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php if(get_field('banner')): ?>

		<section class="banner">
			<div class="wrapper">
				<?php the_field('banner'); ?>
			</div>
		</section>

	<?php endif; ?>

	<section class="hero">
		<div class="logo">
			<div class="logo-wrapper">
				<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>
		
		<div class="photo">
			<div class="content">
				<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />	
			</div>			
		</div>
	</section>

	<section class="links">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('links_headline'); ?></h2>
			</div>

			<div class="links-wrapper">
				<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>

				    <div class="link">
						<?php 

						$link = get_sub_field('link');

						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
								<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						<?php endif; ?>	

						<?php if(get_sub_field('note')): ?>

							<div class="note">
				    			<?php the_sub_field('note'); ?>
				    		</div>

			    		<?php endif; ?>

				    </div>

				<?php endwhile; endif; ?>				
			</div>

		</div>
	</section>

	<section class="photos">
		<div class="wrapper">

			<?php  $images = get_field('gallery'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>
					<div class="photo">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>					
				<?php endforeach; ?>
			<?php endif; ?>		

		</div>
	</section>

	<section class="info">
		<div class="wrapper">
			
			<div class="menus col">
				<div class="headline">
					<h3>menus<strong>.</strong></h3>
				</div>

				<div class="links-wrapper">
					<?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?>
					 
				    	<?php if(get_sub_field('coming_soon')): ?>

				    		<div class="link note">
				    			<p><?php the_sub_field('note'); ?></p>
				    		</div>

				    	<?php else: ?>

						    <div class="link">
								<?php 

								$link = get_sub_field('link');

								if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
								<?php endif; ?>
						    </div>

						<?php endif; ?>	

					<?php endwhile; endif; ?>				
				</div>
			</div>

			<div class="location col">
				<div class="headline">
					<h3>location<strong>.</strong></h3>
				</div>

				<div class="copy">
					<p><a href="<?php the_field('address_link'); ?>" rel="external"><?php the_field('address'); ?></a></p>
				</div>

				<div class="large-party">
					<div class="headline">
						<h3><?php the_field('large_party_headline'); ?><strong>.</strong></h3>
					</div>

					<div class="copy">
						<?php the_field('large_party_copy'); ?>
					</div>
				</div>
			</div>

			<div class="hours col">
				<div class="headline">
					<h3>hours<strong>.</strong></h3>
				</div>

				<div class="copy">
					<p><?php the_field('hours'); ?></p>
				</div>
			</div>

		</div>
	</section>

	<section class="footer-links">
		<div class="wrapper">

			<div class="col-wrapper">
	
				<div class="feeedback col">
					<a class="headline-link" href="mailto:<?php the_field('email'); ?>">feedback<strong>.</strong></a>
				</div>

				<div class="gift-cards col">
					<a class="headline-link" href="<?php the_field('gift_cards_link'); ?>" rel="external">gift cards<strong>.</strong></a>
				</div>

				<div class="social col">
					<div class="links-wrapper">
						<?php if(have_rows('social')): while(have_rows('social')): the_row(); ?>
							<a href="<?php the_sub_field('link'); ?>" rel="external">
								<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						<?php endwhile; endif; ?>
					</div>

				</div>			
	
			</div>

		</div>
	</section>

	<section class="hrg-footer">
		<div class="wrapper">
			
			<p><a href="http://www.heavyrestaurantgroup.com/" rel="external">Heavy Restaurant Group</a></p>

			<div class="newsletter">
				
				<h4>Sign up for our newsletter to get updates on news and events</h4>

				<div class="cta">
					<a href="#" class="btn" id="subscribe">Stay Connected</a>
				</div>

			</div>

		</div>
	</section>

<?php get_footer(); ?>