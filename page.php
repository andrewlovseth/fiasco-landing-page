<?php get_header(); ?>

	<section id="main" class="generic">
		<div class="wrapper">

			<h1><?php the_title(); ?></h1>			
			<?php the_content(); ?>

		</div>
	</section>

<?php get_footer(); ?>