<section id="large-party-form">
	<div class="overlay">
		<div class="overlay-wrapper">
			
			<a href="#" class="form-close close">✕</a>

			<div class="form-wrapper">
				<?php
					$shortcode = get_field('large_party_form_shortcode');
					echo do_shortcode($shortcode);
				?>
			</div>

		</div>
	</div>
</section>