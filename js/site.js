$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Open Newsletter Subscription
	$('#subscribe').on('click', function(){
		$('#newsletter-subscribe').fadeIn(25, function(){
			$('#newsletter-subscribe .overlay').addClass('show');
		});		

		return false;
	});

	// Close Newsletter Subscription
	$('#newsletter-subscribe a.close').on('click', function(){
		$('#newsletter-subscribe .overlay').removeClass('show');
		setTimeout(function() {
			$('#newsletter-subscribe').hide();
		}, 500);
		return false;
	});	



	// Open Form Modal
	$('.form-trigger').on('click', function(){
		$('#large-party-form').fadeIn(25, function(){
			$('#large-party-form .overlay').addClass('show');
		});		

		return false;
	});

	// Close Newsletter Subscription
	$('.form-close').on('click', function(){
		$('#large-party-form .overlay').removeClass('show');
		setTimeout(function() {
			$('#large-party-form').hide();
		}, 500);
		return false;
	});	




});